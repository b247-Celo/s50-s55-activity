
import {Card,Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

export default function CourseCard({course}) {

  //console.log(props);
  //console.log(typeof props);

  const {name, description, price, _id} = course;
  //const [count, setCount] = useState(0);
  //Activity 51
  //const [seats, setSeats] = useState(30);
  //Discussion 52
  //const [isOpen, setIsOpen] = useState(true);

  //console.log(useState(0));

  /*function enroll(){
    if (seats > 0) {
      setCount(count + 1);
      console.log('Enrollees' + count);
      setSeats(seats - 1);
      console.log('Seats' + seats);
  } else {
      alert("No more seats available")
  }
}
*/
//Activity S51 3/22/2023
  /*function enroll(){
    if (count === 0) {
      alert('No more seats available.');
      return;
    }

    setCount(count - 1);
    console.log('Enrollees ' - count);
  };
*/

/*function enroll() {
  setCount(count + 1);
  console.log('Enrollees' + count);
    setSeats(seats - 1);
    console.log('Seats' + seats);
}

useEffect(()=> {
  if (seats === 0){
    setIsOpen(false);
    alert("No more seats available.");
    document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)

  }
}, [seats])*/

  return (
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price</Card.Subtitle>
        <Card.Text>{price}</Card.Text>
        <Button className="bg-primary" as={Link} to= {`/courses/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
    )
}

CourseCard.propTypes = {
  course: PropTypes.shape({
    name:PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}