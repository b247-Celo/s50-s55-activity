import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {useNavigate, Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login() {
	// Allows us to consume the User Context object and it's properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	//const navigate = useNavigate();
	const [email, setEmail] = useState("");
	const [password, setPassword1] = useState("");
	
	
 	const [isActive, setIsActive] = useState("true");

 	function authenticate(e) {

 		e.preventDefault();

 		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
 			method: 'POST',
 			headers:{
 				'Content-Type': 'application/json'
 			},
 			body: JSON.stringify({
 				email: email,
 				password: password
 			})
 		})
 		.then(res=> res.json())
 		.then(data=> {
 			console.log(data)

 			if(typeof data.access !== "undefined"){
 				localStorage.setItem('token', data.access)
 				retrieveUserDetails(data.access);

 				Swal.fire({
 					title: 'Login Successful',
 					icon: 'success',
 					text: 'Welcome to Zuitt!'
 				})
 			} else {
 				Swal.fire({
 					title: 'Authentication Failed',
 					icon: 'error',
 					text: 'Please check your login details and try again!'
 			})
 			}

 		})

 		//localStorage.setItem("email", email)

 		//setUser({ email: localStorage.getItem('email')});

 		setEmail("");
 		setPassword1("");
 		//navigate('/');
 		

 		//alert(`"${email}" has been verified! Welcome back!`);
 	}
 	const retrieveUserDetails = (token) => {

 		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
 			headers: {
 				Authorization : `Bearer ${token}`
 			}
 		})
 		.then(res=> res.json())
 		.then(data=> {
 			console.log(data)

 			setUser({
 				id: data._id,
 				isAdmin: data.isAdmin
 			})
 		})
 	}

 	useEffect(() => {
 		if((email !== "" && password !== "" ) && (password.length >= 1 )){
 			setIsActive(true);
 		} else {
 			setIsActive(false);
 		}
 	}, [email, password]);

	return (
		(user.id !==null) ?
			<Navigate to="/courses"/>
			:

		<Form onSubmit={(e) => authenticate(e)}>
			<h2>Login</h2>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e=> setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>

			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e=> setPassword1(e.target.value)}
					required
				/>
			</Form.Group>	


			{ isActive ?

			<Button variant="success my-3" type="submit" id="submitBtn">Login</Button>
			:
			<Button variant="success my-3" type="submit" id="submitBtn" disabled>Login</Button>
			}
			</Form>	

		)
}