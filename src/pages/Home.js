import Banner from '../components/Banner';
import Highlight from '../components/Highlight';

export default function Home(){

	const data = {
		title: "Zuitt Coding Bootcamp",
		content: "Oppurtunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll Now!"
	}
	
	return (
		<>
			<Banner data={data} />
			<Highlight />
		</>
	)
}