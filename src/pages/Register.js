import {Form, Button} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
//S53 Activity
import UserContext from '../UserContext';
import Swal from "sweetalert2";
import { useNavigate, Navigate } from 'react-router-dom';

export default function Register() {

	// S53 Activity- Allows us to consume the User Context object and it's properties to use for user validation
	const {user} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	
 	const [isActive, setIsActive] = useState(false);

 	const navigate = useNavigate();

 	function registerUser(e) {

 		e.preventDefault();

 		//localStorage.setItem("email", email)

 		//setUser({ email: localStorage.getItem('email')});
 		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			body: JSON.stringify({
				email: email
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: "Duplicate Email found",
					icon: "error",
					text: "Please provide a different Email"
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					body: JSON.stringify({
						email: email,
						password: password1,
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo
					}),
					headers: {
						'Content-Type': 'application/json'
					}
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire({
						title: "Registration successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					})
					navigate("/login");
				});
			};
		});


 		setEmail("");
 		setPassword1("");
 		setPassword2("");
 		setFirstName("");
 		setLastName("");
 		setMobileNo("");

 		//alert('Thank you for registering')
 	}

 	useEffect(() => {
 		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (password1.length && password2.length >= 8 ) && (mobileNo>=11)){
 			setIsActive(true);
 		} else {
 			setIsActive(false);
 		}
 	}, [ firstName, lastName, mobileNo,email, password1, password2]);

	return (
		//S53 Activity -
		(user.id !==null) ?
			<Navigate to="/courses"/>
			
			:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="firstname"
					placeholder="Enter First Name Here"
					value={firstName}
					onChange={e=> setFirstName(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="lastname"
					placeholder="Enter Last Name here"
					value={lastName}
					onChange={e=> setLastName(e.target.value)}
					required
				/>
				
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="mobileNo"
					placeholder="Enter Mobile Number here"
					value={mobileNo}
					onChange={e=> setMobileNo(e.target.value)}
					required
				/>
				
			</Form.Group>	

			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e=> setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password1}
					onChange={e=> setPassword1(e.target.value)}
					required
				/>
			</Form.Group>	

			<Form.Group controlId="password2">
				<Form.Label> Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e=> setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			

			{ isActive ?

			<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
			:
			<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
			}
			</Form>	

		)
}