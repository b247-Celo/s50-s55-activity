//import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import { useState, useEffect } from 'react';

export default function Courses() {

	//Checks if the mock data was captured
	// console.log(coursesData);
	// console.log(coursesData[0]);

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course={course} />
	// 		)
	// })

	const [ courses, setCourses ] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res=>  res.json())
		.then(data => {
			console.log(data)
			
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} course={course} /> )
			}))
		})
	}, [])

	return(
		<>
		{courses}
		</>
		)
}