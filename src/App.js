import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import CourseView from './components/CourseView';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import './App.css';

import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';

function App() {

  const [user, setUser] = useState({
    id:null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=> res.json())
    .then(data=> {
      if(typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
  })
    } else {
      setUser ({
        id:null,
        isAdmin:null
      })
    }
})
  }, [])
  return (
    
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavbar />
        <Container>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses/>} />
            <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            {/*
              "*" - is a wildcard character that will match any path that has not already been matched by previous routes.
            */}
            <Route path="*" element={<Error/>} />
          </Routes>
        </Container>
    </Router>
    </UserProvider>

    </>
      
    
  );
}

export default App;
