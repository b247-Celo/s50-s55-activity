import React from 'react';

// Initilizes a React Context
// Creates a Context Object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app
// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling
	// Prop-Drilling, is the term we use whenever we pass information from one component to another using props
const UserContext = React.createContext();

// Initilizes a Context Provider
// Give us ability to provide specific context through component
// The 'Provider' components allows other components to consume/use/utilize/facilitate context object and supply the necessary information needed to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;